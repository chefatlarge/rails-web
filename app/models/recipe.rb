class Recipe < ApplicationRecord
  belongs_to :user
  belongs_to :category
  has_many   :attachments
  has_many   :comment_blocks
  has_many   :post_collaborators
  has_many   :post_collaborators_users, through: :post_collaborators, :source => :user
  extend FriendlyId
  ratyrate_rateable 'title'

  has_paper_trail
  friendly_id :title, use: :slugged
  acts_as_taggable
  after_save :destroy_post_collaborators
  has_attached_file :attachment, :styles => { :medium => "300x300>", :thumb => "100x100#" }
  validates_attachment_content_type :attachment, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  include PgSearch
  multisearchable against: [:title, :name, :content]
  validates :title, presence: true
  after_save :reindex

  def should_generate_new_friendly_id?
    slug.blank? || name_changed? || new_record?
  end

  def similar_posts(limit = 5)
    subquery = Recipe.select('id, title').where(:id => self.id).as('original').to_sql.sub('$1', self.id.to_s)
    similar =  Recipe.where(post_status: "publish").select("recipes.*, ts_rank_cd(to_tsvector('english', recipes.title), replace(plainto_tsquery(original.title)::text, ' & ', ' | ')::tsquery, 8) AS similarity").
        from("recipes, #{subquery}").
        where('recipes.id != original.id').
        order('similarity DESC').
        limit(limit)
    similar
  end

  protected
  def destroy_post_collaborators
    post =  @recipes = Recipe.find(self.id)
    if  post.post_status == 'publish'
      self.post_collaborators.delete_all
    end
  end

  def reindex
    PgSearch::Multisearch.rebuild(Recipe)
  end

  def self.search(terms = "")
    sanitized = sanitize_sql_array(["to_tsquery('english', ?)", terms.gsub(/\s/,"+")])
    Recipe.where("tsv @@ #{sanitized}")
  end
end
