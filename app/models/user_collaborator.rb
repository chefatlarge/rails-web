class UserCollaborator < ApplicationRecord
  belongs_to :user
  belongs_to :user_collaborator, :class_name => :User, :foreign_key => :post_collaborator_id

  validates :user_id, :presence => true, :uniqueness => {:scope => :post_collaborator_id}
end
