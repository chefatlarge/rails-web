class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise   :database_authenticatable, :registerable,:async,
           :recoverable, :rememberable, :trackable, :validatable,
           :omniauthable, :omniauth_providers => [:facebook]
  has_many :comment_blocks
  has_many :posts
  has_many :recipes
  has_many :pages
  has_many :user_collaborators
  has_many :post_collaborators, through: :user_collaborators
  has_many :foodiye_posts
  belongs_to :role
  ratyrate_rater
  before_create :set_default_role
  include PgSearch

  pg_search_scope :profile_search,
                  :against => [:user_nicename, :email],
                  :using => {
                    dmetaphone: {
                        dictionary: 'english',
                        tsvector_column: 'tsv_users'
                    },
                    tsearch: {
                        dictionary: 'english',
                        tsvector_column: 'tsv_users'
                    }
                   }

  has_attached_file :avatar, :styles => { :medium => "1000x1000>", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  include Likeable::UserMethods
  # validates :email, presence: true
  # validates :password, presence: true
  # validates :user_nicename, presence: true, uniqueness: true


  def self.from_omniauth(auth)
    if self.where(email: auth.info.email).exists?
      return_user = self.where(email: auth.info.email).first
      return_user.provider = auth.provider
      return_user.uid = auth.uid
    else
      return_user = self.where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.user_nicename = auth.info.name
        user.fb_image_url = auth.info.image
        user.email = auth.info.email
        user.password = Devise.friendly_token[0,20]
      end
    end
    puts return_user
    return_user
  end

  def admin?
    role.try(:name) == "admin"
  end

  def editor?
    role.try(:name) == "editor"
  end

  def writer?
    role.try(:name) == "writer"
  end

  def normal?
    role.try(:name) == "normal"
  end

  private
  def set_default_role
    self.role ||= Role.find_by_name('normal')
  end


end
