class WpTerm < ApplicationRecord
  establish_connection(:wp_database)
  self.table_name = 'c4l_terms'
  self.primary_key = 'term_id'
  has_many :wp_post_term_taxonomies, :foreign_key => :term_taxonomy_id
  has_many :wp_posts, through: :wp_post_term_taxonomies
  has_one :wp_term_taxonomy, primary_key: :term_id, foreign_key: :term_id
end
