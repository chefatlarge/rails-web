class FoodiyePost < ApplicationRecord
  belongs_to :user
  has_many   :comment_blocks
  has_attached_file :attachment, :styles => { :medium => "300x300>", :thumb => "100x100#" }
  validates_attachment_content_type :attachment, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  validates :title, :description, presence: true
  validates :video_url, presence: true, :unless => lambda { |o| o.post_type != 'video' }
  validates :photo_link, presence: true, :unless => lambda { |o| o.post_type != 'photo' }, if: :attachment_file_nil?
  include Likeable

  protected
  def attachment_file_nil?
    self.attachment_file_size == nil
  end

end
