class Category < ApplicationRecord
  has_many   :posts
  has_many   :recipes
  has_many   :pages
  belongs_to :category

end
