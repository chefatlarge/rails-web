class WpAuthor < ApplicationRecord
  establish_connection(:wp_database)
  self.table_name = 'c4l_users'
  self.primary_key = 'ID'
  has_many :wp_posts, :foreign_key => :post_author

end
