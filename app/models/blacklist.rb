class Blacklist < ApplicationRecord
  validates :domain, presence: true
end
