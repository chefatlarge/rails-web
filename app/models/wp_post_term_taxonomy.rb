class WpPostTermTaxonomy < ApplicationRecord
  establish_connection(:wp_database)
  self.table_name = 'c4l_term_relationships'
  self.primary_keys = :object_id, :term_taxonomy_id
  belongs_to :wp_post, foreign_key: :object_id
  belongs_to :wp_term_taxonomy, foreign_key: :term_taxonomy_id
end
