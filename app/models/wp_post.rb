class WpPost < ApplicationRecord
  establish_connection(:wp_database)
  self.table_name = 'c4l_posts'
  self.primary_key = 'ID'
  has_many :wp_post_term_taxonomies, :foreign_key => :object_id
  has_many :wp_term_taxonomies, through: :wp_post_term_taxonomies
  belongs_to :wp_authors

  def categories
    wp_term_taxonomies.where(taxonomy: 'category').map { |t| t.term_id }
  end
  def tags
    wp_term_taxonomies.where(taxonomy: 'post_tag').map { |t| t.term_id }
  end
end