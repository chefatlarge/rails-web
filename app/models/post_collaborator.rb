class PostCollaborator < ApplicationRecord
  belongs_to :post
  belongs_to :recipe
  belongs_to :user_collaborator
  belongs_to :user
  validates  :post_id, :allow_blank => true, :uniqueness => {:scope => :user_collaborator_id}
  validates  :recipe_id, :allow_blank => true, :uniqueness => {:scope => :user_collaborator_id}
end
