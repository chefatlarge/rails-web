class Attachment < ApplicationRecord
  has_attached_file :attachment, styles: { medium: "300x300>", thumb: "100x100>" },
                    default_url: "/images/:style/missing.png"
  validates_attachment_content_type :attachment, content_type: ['image/jpeg','image/bmp', 'image/png','image/tiff', 'image/gif', 'application/pdf','application/json']
  belongs_to :post
  belongs_to :page
  belongs_to :recipe
  belongs_to :user
end
