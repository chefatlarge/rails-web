class Ability
  include CanCan::Ability
  def initialize(user)
     user ||= User.new # for guest

    unless user
      can :read, :all #for guest without roles
    end

    if user.admin?

      can [:read, :show, :new, :create, :update, :destroy], [Post, PostCollaborator, Recipe]
      can [:new, :create], [CommentBlock, UserCollaborator, PostCollaborator]
      can [:show, :destroy], User, :id => user.id
      can [:authors, :tags, :categories, :undo_link, :secret_url], Post
      can [:secret_url_recipe, :tags, :categories, :undo_link, :edit], Recipe
    elsif user.editor?
      can [:read, :show, :new, :create], [Post, PostCollaborator, Recipe]
      can [:new, :create], [CommentBlock, UserCollaborator, PostCollaborator]
      can [:show, :destroy], User, :id => user.id
      can [:authors, :tags, :categories, :undo_link], Post
    elsif user.writer?
      can [:read], [Post, PostCollaborator, UserCollaborator]
      can [:show, :destroy], User, :id => user.id
      can [:authors, :tags, :categories, :undo_link], Post
    else  user.normal?
      can [:read], [Post, Recipe]
      can [:show, :destroy], User, :id => user.id
      can [:authors, :tags, :categories], Post
    end
  end
end
