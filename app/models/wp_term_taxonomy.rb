class WpTermTaxonomy < ApplicationRecord
  establish_connection(:wp_database)
  self.table_name = 'c4l_term_taxonomy'
  self.primary_key = 'term_taxonomy_id'
  belongs_to :wp_term, foreign_key: :term_id
end
