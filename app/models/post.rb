class Post < ApplicationRecord
  belongs_to :user
  has_many :attachments
  belongs_to :category
  has_many :comment_blocks
  has_many :post_collaborators
  has_many :post_collaborators_users, through: :post_collaborators, :source => :user
  extend FriendlyId
  has_paper_trail
  friendly_id :generate_url, use: :slugged
  acts_as_taggable
  validates :title, presence: true
  after_save :destroy_post_collaborators
  include PgSearch
  multisearchable against: [:title, :name, :content]
  after_save :reindex
  def similar_posts(limit = 5)
    subquery = Post.unscoped.select('id, title').where(:id => self.id).as('original').to_sql.sub('$1', self.id.to_s)
    similar =  Post.where(post_status: "publish").select("posts.*, ts_rank_cd(to_tsvector('english', posts.title), replace(plainto_tsquery(original.title)::text, ' & ', ' | ')::tsquery, 8) AS similarity").
      from("posts, #{subquery}").
      where('posts.id != original.id').
      order('similarity DESC').
      limit(limit)
    similar
  end

  protected

  def generate_url
    "#{name}-#{id}"
  end

  def should_generate_new_friendly_id?
    name_changed?
  end

  def destroy_post_collaborators
    post =  @post = Post.find(self.id)

    if  post.post_status == 'publish'
      self.post_collaborators.delete_all
    end
  end

  def reindex
    PgSearch::Multisearch.rebuild(Post)
  end

  # def self.search(terms = "")
  #   sanitized = sanitize_sql_array(["to_tsquery('english', ?)", terms.gsub(/\s/,"+")])
  #   Post.where("tsv @@ #{sanitized}")
  # end
end
