json.array!(@post_collaborators) do |post_collaborator|
  json.extract! post_collaborator, :id
  json.url post_collaborator_url(post_collaborator, format: :json)
end
