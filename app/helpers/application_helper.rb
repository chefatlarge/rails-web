module ApplicationHelper
  def markdown(text)
    options = {
        filter_html:     true,
        hard_wrap:       true,
        link_attributes: { rel: 'nofollow', target: "_blank" },
        space_after_headers: true,
        fenced_code_blocks: true,
    }
    extensions = {
        quote: true,
        autolink:           true,
        superscript:        true,
        disable_indented_code_blocks: true
    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end

  def check_current_user?
    if user_signed_in?
      if current_user.role.name == "admin" || current_user.role.name == "editor" || current_user.role.name == "writer"
      return true
      end
    end
  end

  def if_admin?
    if user_signed_in?
      if current_user.role.name == "admin" || current_user.role.name == "editor"
        return true
      end
    end
  end

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end
  def resource_class
    devise_mapping.to
  end
end
