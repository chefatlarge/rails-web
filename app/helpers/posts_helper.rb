module PostsHelper
  def article_status_all
    if current_user.role.name == "admin" || current_user.role.name == "editor"
      article_status = {'in progress' => 'in progress',
                        'ready for publish' =>'ready for publish',
                        'published' => 'publish',
                        'rejected' => 'rejected'}
    else
      article_status = {'in progress' => 'in progress',
                        'ready for publish' =>'ready for publish'}
    end
  end

  def users_all
    users = User.all
    users.each do |user|
      user.email
    end
  end

  def check_post_coll_user
    post_collaborators = PostCollaborator.where(post_id: @post)
    post_collaborators.each do |post_collaborator|
      if @current_user.id == post_collaborator.user_collaborator_id
        return true
      else
        return false
      end
    end
  end

end
