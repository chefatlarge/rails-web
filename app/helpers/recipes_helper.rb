module RecipesHelper

  def check_recipe_coll_user
    post_collaborators = PostCollaborator.where(recipe_id: @recipe)
    post_collaborators.each do |post_collaborator|
      if @current_user.id == post_collaborator.user_collaborator_id
        return true
      else
        return false
      end
    end
  end
end
