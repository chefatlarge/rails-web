module LikesHelper
  def like_link_for(foodiye_post)
    link_to "Like", like_path(:resource_name => foodiye_post.class, :resource_id => foodiye_post.id), :method => :post
  end

  def unlike_link_for(foodiye_post)
    link_to "Unlike", like_path(:resource_name => foodiye_post.class, :resource_id => foodiye_post.id), :method => :delete
  end
end
