class UsersMailer < ApplicationMailer

  default    from: "Chefatlarge <postmaster@sandbox4c908791028645a7a999dc653023cc7e.mailgun.org>",
             template_path: 'mailers/users'

  def contact_detail_recipient(user, mail_recipient)
    @user = user
    mail to: mail_recipient
  end

  def contact_detail_current_user(user, mail_recipient)
    @user = user
    @mail_recipient = mail_recipient
    mail to: user.email
  end

end