class PostsMailer < ApplicationMailer

  default    from: "Chefatlarge <postmaster@mailers.chefatlarge.in>",
             template_path: 'mailers/posts'

  def post_updated(post)
    mail to: post_coll_find(post)
  end

  def comment_updated(post)
    mail to: post_coll_find(post)
  end

  def assigned_updated(post)
    mail to: post_coll_find(post)
  end

  def post_coll_find(post)
    @post = post
    mails = []
    post.post_collaborators.each do |post_collaborator|
    u = User.find(post_collaborator.user_collaborator_id)
    mails.push(u.email)
    end
    mails
  end
end
