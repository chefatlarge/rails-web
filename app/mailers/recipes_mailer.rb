class RecipesMailer < ApplicationMailer
  default    from: "Chefatlarge <postmaster@sandbox4c908791028645a7a999dc653023cc7e.mailgun.org>",
             template_path: 'mailers/recipes'

  def recipe_updated(recipe)
    @recipe = recipe
    mail to: post_coll_find(recipe)
  end

  def comment_updated(recipe)
    @recipe = recipe
    mail to: post_coll_find(recipe)
  end

  def assigned_updated(recipe)
    @recipe = recipe
    mail to: post_coll_find(recipe)
  end

  def post_coll_find(recipe)
    mails = []
    recipe.post_collaborators.each do |post_collaborator|
      u = User.find(post_collaborator.user_collaborator_id)
      mails.push(u.email)
    end
    mails
  end

end
