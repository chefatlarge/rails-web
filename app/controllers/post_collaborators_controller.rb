class PostCollaboratorsController < ApplicationController
  #load_resource :find_by => :slug
  authorize_resource

  before_action :set_post_collaborator, only: [:show, :edit, :update, :destroy]

  def index
    @post_collaborators = PostCollaborator.all
  end

  def show
  end

  def new
    @post_collaborator = PostCollaborator.new
  end

  def edit
  end

  def create
    if params[:post_collaborator][:post_id]
      @post = Post.find(params[:post_collaborator][:post_id])
      @post_collaborator = PostCollaborator.new(post_collaborator_params)
      collaborate_user = User.find(@post_collaborator.user_collaborator_id)
      respond_to do |format|
        if @post_collaborator.save
        flash[:notice] = "You have been added as collaborator"
          @auto_comment = @post.comment_blocks.new
          @auto_comment.user = current_user
          @auto_comment.body = " has been added as collaborator"
          @auto_comment.collaborate_user = collaborate_user.email
          @auto_comment.save!
          PostsMailer.assigned_updated(@post).deliver_later
          format.html { redirect_to :back }
        else
          format.html { redirect_to :back, text: "Collaborator not added" }
        end
      end

    else
      @recipe = Recipe.find(params[:post_collaborator][:recipe_id])
      @post_collaborator = PostCollaborator.new(post_collaborator_params)
      collaborate_user = User.find(@post_collaborator.user_collaborator_id)
      respond_to do |format|
        if @post_collaborator.save
          flash[:notice] = "You have been added as collaborator"
          @auto_comment = @recipe.comment_blocks.new
          @auto_comment.user = current_user
          @auto_comment.body = " has been added as collaborator"
          @auto_comment.collaborate_user = collaborate_user.email
          @auto_comment.save!
          RecipesMailer.assigned_updated(@recipe).deliver_later
          format.html { redirect_to :back }
        else
          format.html { redirect_to :back, text: "Collaborator not added" }
        end
      end
    end
  end

  def update
    respond_to do |format|
      if @post_collaborator.update(post_collaborator_params)
        format.html { redirect_to :back, notice: 'Post collaborator was successfully updated.' }
        format.json { render :show, status: :ok, location: @post_collaborator }
      else
        format.html { render :edit }
        format.json { render json: @post_collaborator.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
      postcollaborator = PostCollaborator.find(params[:id])
      postcollaborator.destroy
        unless postcollaborator.post_id.nil?
          user_id = postcollaborator.user_collaborator_id
          post_id = postcollaborator.post_id
          post_id = Post.find(post_id)
          user = User.find(user_id)
        else
          user_id = postcollaborator.user_collaborator_id
          recipe_id = postcollaborator.recipe_id
          recipe_id = Recipe.find(recipe_id)
          user = User.find(user_id)
        end
      respond_to do |format|
        format.html { redirect_to :back, notice: 'Post collaborator was successfully destroyed.' }
        format.json { head :no_content }
          unless postcollaborator.post_id.nil?
            @auto_comment = CommentBlock.new
            @auto_comment.post_id = post_id.id
            @auto_comment.user = current_user
            @auto_comment.body = " has been deleted from collaborators"
            @auto_comment.collaborate_user = user.email
            @auto_comment.save!
          else
            @auto_comment = CommentBlock.new
            @auto_comment.recipe_id = recipe_id.id
            @auto_comment.user = current_user
            @auto_comment.body = " has been deleted from collaborators"
            @auto_comment.collaborate_user = user.email
            @auto_comment.save!
          end
      end

  end



  private
    def set_post_collaborator
      @post_collaborator = PostCollaborator.find(params[:id])
    end

    def post_collaborator_params
      params.fetch(:post_collaborator).permit!
    end
end
