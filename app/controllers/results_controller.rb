class ResultsController < ApplicationController

  def index
      if params[:search] == 'posts_recipes'
        @search_results = PgSearch.multisearch(params[:query])
                            .paginate(:page => params[:page], :per_page => 3)
      elsif params[:search] == 'profiles'

        @search_results =  User.profile_search(params[:query])
                            .paginate(:page => params[:page], :per_page => 3)
      end
      correct_search = Spellchecker.check(params[:query])
      correct_search.map{|x| @correct = x[:correct]}
      if @correct == false
        correct_search.map{|x| @correct_result = x[:suggestions]}
      end
  end
end
