class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  before_filter :authenticate_user!, :except => [:show, :index, :authors,:tags, :categories]
  before_filter :set_paper_trail_whodunnit
  before_action :configure_permitted_parameters, if: :devise_controller?
  def after_sign_in_path_for(user)
    root_path
  end
  before_filter do
    resource = controller_path.singularize.gsub('/', '_').to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:user_nicename, :phone_number, :facebook_link, :instagram_link, :linkedin_link, :pinterest_link, :twitter_link, :public, :short_description, :avatar])
    devise_parameter_sanitizer.permit(:sign_up, keys: [:user_nicename])
  end

  def after_sign_out_path_for(resource_or_scope)
      :back
  end
end
