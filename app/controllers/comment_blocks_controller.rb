class CommentBlocksController < ApplicationController

  def create
     if params[:post_id]
      @post = Post.find_by_slug(params[:post_id])
      @comment = @post.comment_blocks.new(comment_params)
      @comment.user = current_user
      @comment.save!
    elsif params[:recipe_id]
      @recipe = Recipe.find_by_slug(params[:recipe_id])
      @comment = @recipe.comment_blocks.new(comment_params)
      @comment.user = current_user
      @comment.save!
    elsif params[:foodiye_post_id]
       @foodiye_post = FoodiyePost.find_by_id(params[:foodiye_post_id])
       @comment = @foodiye_post.comment_blocks.new(comment_params)
       @comment.user = current_user
       @comment.save!
    end
      redirect_to :back
      PostsMailer.comment_updated(@post).deliver_later
      PostsMailer.comment_updated(@recipe).deliver_later
  end

  private
  def comment_params
    params.require(:comment_block).permit!
  end
end
