class UserCollaboratorsController < ApplicationController
  load_resource :find_by => :slug
  authorize_resource

  before_action :set_user_collaborator, only: [:show, :edit, :update, :destroy]

  # GET /user_collaborators
  # GET /user_collaborators.json
  def index
    @user_collaborators = UserCollaborator.all
    redirect_to root_path
  end

  # GET /user_collaborators/1
  # GET /user_collaborators/1.json
  def show
    redirect_to root_path
  end

  # GET /user_collaborators/new
  def new
    @user_collaborator = UserCollaborator.new
  end

  # GET /user_collaborators/1/edit
  def edit
    redirect_to root_path
  end

  # POST /user_collaborators
  # POST /user_collaborators.json
  def create
    @user_collaborator = UserCollaborator.new(user_collaborator_params)
    if @user_collaborator.save
      redirect_to :back
    else
      redirect_to :back
    end
  end

  # PATCH/PUT /user_collaborators/1
  # PATCH/PUT /user_collaborators/1.json
  def update
    if @user_collaborator.update(user_collaborator_params)
      redirect_to :back
    else
      redirect_to :back
    end
  end

  # DELETE /user_collaborators/1
  # DELETE /user_collaborators/1.json
  def destroy
    @user_collaborator.destroy
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_collaborator
      @user_collaborator = UserCollaborator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_collaborator_params
      params.require(:user_collaborator).permit(:post_collaborator_id, :user_id, :commit)
    end
end
