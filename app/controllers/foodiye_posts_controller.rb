class FoodiyePostsController < ApplicationController
  require 'uri/http'
  require 'nokogiri'
  require 'open-uri'

  layout 'foodiye'
  before_action :set_foodiye_post, only: [:show, :edit, :update, :destroy]
  skip_before_filter :verify_authenticity_token, :only => [:index, :show]
  before_action :authenticate_user!, :except => [:show, :index  ]
  before_action :require_permission, :only => [:edit, :update]
  before_action :destroy_if_foodiye_admin, :only => [:destroy]
  before_action :check_blacklist, :only => [:create, :update]

  # GET /foodiye_posts
  def index
    @foodiye_posts = FoodiyePost.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 20)
  end

  # GET /foodiye_posts/1
  def show
    @foodiye_post = FoodiyePost.find(params[:id])
  end

  # GET /foodiye_posts/new
  def new
    @foodiye_post = FoodiyePost.new
  end

  # GET /foodiye_posts/1/edit
  def edit
    @foodiye_posts_comment = CommentBlock.where(foodiye_post_id: @foodiye_post).order('created_at DESC')
  end

  # POST /foodiye_posts
  def create
    @foodiye_post = FoodiyePost.new(foodiye_post_params)
    # check_blacklist(@foodiye_post.photo_link.to_s)
    respond_to do |format|
      if @foodiye_post.save
        unless @foodiye_post.video_url.blank?
          convert = @foodiye_post.video_url.gsub('watch?v=', 'embed/')
          @foodiye_post.video_url = convert
          @foodiye_post.save!
        end
        format.html { redirect_to @foodiye_post, notice: 'Foodiye post was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /foodiye_posts/1
  def update
    respond_to do |format|
      if @foodiye_post.update(foodiye_post_params)
        unless @foodiye_post.video_url.blank?
          convert = @foodiye_post.video_url.gsub('watch?v=', 'embed/')
          @foodiye_post.video_url = convert
          @foodiye_post.save!
        end
        format.html { redirect_to @foodiye_post, notice: 'Foodiye post was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /foodiye_posts/1
  def destroy
    @foodiye_post.destroy
    respond_to do |format|
      format.html { redirect_to foodiye_posts_url, notice: 'Foodiye post was successfully destroyed.' }
    end
  end

  def members
    @foodiye_posts = FoodiyePost.all.order("created_at DESC")
    @users = User.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 30)
  end

  def get_title
    url = params[:data_value]
    doc = Nokogiri::HTML(open(url))
    doc.at_css('title') ? title = doc.at_css('title').text : title = "no title suggested"
    title
    if title != "no title suggested"
      data = {:message => title}
      render :json => data, :status => :ok
    else
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_foodiye_post
      @foodiye_post = FoodiyePost.find(params[:id])
    end

  def check_blacklist
    if action_name == "new" or action_name == "create"
      @foodiye_post = FoodiyePost.new(foodiye_post_params)
    else  action_name == "update"
       @foodiye_post.assign_attributes(params[:foodiye_post])
    end

    blacklist = Blacklist.all
    array = []
    blacklist.each {|name| array.push(name.domain) }
    unless @foodiye_post.photo_link.blank?
      myUri = URI.parse(@foodiye_post.photo_link.to_s).host
    else @foodiye_post.photo_link.blank?
      myUri = URI.parse(@foodiye_post.video_url.to_s).host
    end
    array.each do |black_url|
      if myUri == black_url
        redirect_to :back, notice: 'Can`t save. URL found in blacklist!'
      else
      end
    end
  end

  def require_permission
     if current_user.id != FoodiyePost.find(params[:id]).user_id
        redirect_to foodiye_posts_path
        #Or do something else here
     end
  end

  def destroy_if_foodiye_admin
    unless current_user.role_id == 2
      redirect_to foodiye_posts_path, notice: 'You have no permitions'
    end
  end

    # Never trust parameters from the scary internet, only allow the white list through.
    def foodiye_post_params
      params.require(:foodiye_post).permit!
    end
end
