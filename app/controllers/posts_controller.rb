class PostsController < ApplicationController
  before_action :correct_user, :only => [ :update, :destroy]
  load_resource :find_by => :slug
  authorize_resource
  skip_authorize_resource :only => [ :update, :destroy]

  def index
    today = Date.today
    if params[:tag]
      @posts = Post.tagged_with(params[:tag])
    else
      @posts = Post.where(post_status: "publish").where("schedule_date <= ?", today).order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
  end

  def show
    today = Date.today
    if params[:post_status] == "publish" and params[:schedule_date] <= today
      @post = Post.friendly.find(params[:id])
    else
      if secret_url
      end
    end
  end

  def new
    @post_collaborator = PostCollaborator.new
    @user_collaborator = UserCollaborator.new
    @post = Post.new
  end

  def edit
    if params[:version_id]
      @post_version = PaperTrail::Version.find_by_id(params[:version_id])
      @post = @post_version.reify
    else
      @post = Post.find_by_slug(params[:id])
    end
    @comment_block = CommentBlock.new
    @post_comments = CommentBlock.where(post_id: @post).order('created_at DESC').paginate(:page => params[:page], :per_page => 3)
    @post_collaborator = PostCollaborator.new
    @user_collaborator = UserCollaborator.new
    @gen = SecureRandom.hex(10)
    @secret_url = "#{request.base_url}#{post_path(@post)}?secret=#{@post.secret}"
  end

  def create
    @post = Post.new(post_params)
     user = current_user
     @post.user = user

    respond_to do |format|
      if @post.save
        @post_collaborator = PostCollaborator.new
        @post_collaborator.post_id = @post.id
        @post_collaborator.user_collaborator_id = current_user.id
        @post_collaborator.save!
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
        unless params[:schedule_box].blank?
          @post.schedule_date = Date.today
          @post.save!
        end
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @post = Post.find_by_slug(params[:id])
    @old_post = Post.find_by_slug(params[:id])
    if @post.update(post_params)
      unless @old_post.post_status == @post.post_status
        @auto_comment = @post.comment_blocks.new
        @auto_comment.user = current_user
        @auto_comment.body = "Post status has been changed to #{@post.post_status}"
        @auto_comment.save!
        else
      @auto_comment = @post.comment_blocks.new
      @auto_comment.user = current_user
      @auto_comment.body = 'Post was successfully updated'
      @auto_comment.save!
      if params[:schedule_box].blank?
        @post.schedule_date = Date.today
        @post.save!
      end
      end
      PostsMailer.post_updated(@post).deliver_later
      unless @old_post.secret == @post.secret
        redirect_to edit_post_path(@post)
      else
        redirect_to @post
      end
    else
      render 'edit'
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
      #PostsMailer.post_destroyed(@post).deliver_later
    end
  end

  def authors
    @author = User.find_by_user_nicename(params[:user_nicename])
    @posts = Post.where(user: @author).paginate(:page => params[:page], :per_page => 3)
  end

  def tags
    @tag = Tag.find_by_slug(params[:slug])
    @post_id = @tag.post_ids
    @posts = Post.where(id: @post_id).paginate(:page => params[:page], :per_page => 3)
  end

  def categories
    @categories = Category.find_by_slug(params[:slug])
    @posts = Post.where(category: @categories).paginate(:page => params[:page], :per_page => 3)
  end

  def undo_link
    view_context.link_to("undo", revert_version_path(@post.versions.scoped.last), :method => :post)
  end

  def secret_url
    if @post.post_status == "in progress" || @post.post_status == "ready for publish"
      if @post.secret == params[:secret]
        @post = Post.friendly.find(params[:id])
      else
        redirect_to root_path
      end
    end
  end

  private
   def post_params
     params.require(:post).permit!
   end

  def destroy_post_collaborators
    if  @post.post_status == 'publish'
      self.post_collaborators.delete_all
    end
  end

  def correct_user
    post = Post.find_by_slug(params[:id])
    post_collaborators = PostCollaborator.where(post_id: post)
    has_access = false
    post_collaborators.each do |post_collaborator|
      if post.user_id == @current_user.id
        has_access = true
      elsif @current_user.id == post_collaborator.user_collaborator_id
        has_access = true
      end
    end

    redirect_to root_path, notice:  "Access denied, you must be collaborator" unless has_access
  end
end

