class UsersController < ApplicationController

  def show
    begin
      @user = User.find(params[:id])
      if @user.public == true || current_user == @user
        render "users/show"
        if verify_recaptcha
          UsersMailer.contact_detail_recipient(@user, params[:mail]).deliver_later
          UsersMailer.contact_detail_current_user(@user, params[:mail]).deliver_later
        else
        end
      else
        flash[:notice] = "This is profile not public"
        render "users/not_show"
      end
    rescue ActiveRecord::RecordNotFound
      redirect_to root_path
    end
  end

end
