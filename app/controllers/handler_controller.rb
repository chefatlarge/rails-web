class HandlerController < ApplicationController
  def hand
    attachment = Attachment.find(params[:id])
    redirect_to attachment.attachment.url
  end
end
