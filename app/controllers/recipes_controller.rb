class RecipesController < ApplicationController
  before_action :correct_user, :only => [:update, :destroy]
  load_resource :find_by => :slug
  authorize_resource
  skip_authorize_resource :only => [:update, :destroy]

  # GET /recipes
  def index
    today = Date.today
    if params[:tag]
      @recipes = Recipe.tagged_with(params[:tag])
    else
      @recipes = Recipe.where(post_status: "publish").where("schedule_date <= ?", today).order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    end
  end

  # GET /recipes/1
  def show
    today = Date.today
    if params[:post_status] == "publish" and params[:schedule_date] <= today
      @recipe = Recipe.find_by_slug(params[:id])
    else
      if secret_url_recipe
      end
    end
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
    @post_collaborator = PostCollaborator.new
    @user_collaborator = UserCollaborator.new
  end

  # GET /recipes/1/edit
  def edit
    if params[:version_id]
      @recipe_version = PaperTrail::Version.find_by_id(params[:version_id])
      @recipe = @recipe_version.reify
    else
      @recipe = Recipe.find_by_slug(params[:id])
    end
    @comment_block = CommentBlock.new
    @post_comments = CommentBlock.where(recipe_id: @recipe).order('created_at DESC').paginate(:page => params[:page], :per_page => 3)
    @post_collaborator = PostCollaborator.new
    @user_collaborator = UserCollaborator.new
    @gen = SecureRandom.hex(10)
    @secret_url = "#{request.base_url}#{recipe_path(@recipe)}?secret=#{@recipe.secret}"
  end

  # POST /recipes
  def create
    @recipe = Recipe.new(recipe_params)
    user = current_user
    @recipe.user = user
    respond_to do |format|
      if @recipe.save
        @post_collaborator = PostCollaborator.new
        @post_collaborator.recipe_id = @recipe.id
        @post_collaborator.user_collaborator_id = current_user.id
        @post_collaborator.save!
        if params[:schedule_box].blank?
          @recipe.schedule_date = Date.today
          @recipe.save!
        end
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render :show, status: :created, location: @recipe }
      else
        format.html { render :new }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes/1
  def update
    @recipe = Recipe.find_by_slug(params[:id])
    @old_recipe = Recipe.find_by_slug(params[:id])
    if @recipe.update(recipe_params)
      unless @old_recipe.post_status == @recipe.post_status
        @auto_comment = @recipe.comment_blocks.new
        @auto_comment.user = current_user
        @auto_comment.body = "recipe status has been changed to #{@recipe.post_status}"
        @auto_comment.save!
      else
        @auto_comment = @recipe.comment_blocks.new
        @auto_comment.user = current_user
        @auto_comment.body = 'Recipe was successfully updated'
        @auto_comment.save!
      end
      unless params[:schedule_box].blank?
        @recipe.schedule_date = Date.today
        @recipe.save!
      end
      RecipesMailer.recipe_updated(@recipe).deliver_later
      unless @old_recipe.secret == @recipe.secret
        redirect_to edit_recipe_path(@recipe)
      else
        redirect_to @recipe
      end
    else
      render 'edit'
    end
  end

  # DELETE /recipes/1
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to recipes_url, notice: 'Recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def secret_url_recipe
    if @recipe.post_status == "in progress" || @recipe.post_status == "ready for publish"
      if @recipe.secret == params[:secret]
        @recipe = Recipe.find_by_slug(params[:id])
      else
        redirect_to root_path
      end
    end
  end

  private
    def recipe_params
      recipe = params.require(:recipe).permit!

      if recipe[:ingredient].blank?
        recipe[:ingredient] = {}
      end

      recipe[:ingredient].delete_if do |key, value|
        value[:quantity].blank? && value[:mera].blank? && value[:name].blank? && value[:extra].blank?
      end

      if recipe[:instruction].blank?
        recipe[:instruction] = {}
      end

      recipe[:instruction].delete_if do |key, value|
        value[:step].blank?
      end

      recipe
    end


  def correct_user
    post = Recipe.find_by_slug(params[:id])
    post_collaborators = PostCollaborator.where(recipe_id: post)
    has_access = false
    post_collaborators.each do |post_collaborator|
      if post.user_id == @current_user.id
        has_access = true
      elsif @current_user.id == post_collaborator.user_collaborator_id
        has_access = true
      end
    end

    redirect_to root_path, notice:  "Access denied, you must be collaborator" unless has_access
  end
end
