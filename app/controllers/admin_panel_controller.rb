class AdminPanelController < ApplicationController

  def index

  end

  def new
    @post_collaborator = PostCollaborator.new
    @user_collaborator = UserCollaborator.new
  end

  def main_page

    filter = params[:filter]
    if filter.blank?
      @posts = Post.all
                   .order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    elsif filter == 'all_posts'
      @posts = Post.all
                   .order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    elsif filter == 'my_posts'
      @posts = Post.where(user_id: current_user.id)
                   .order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    elsif filter == 'all_recipes'
      @recipes = Recipe.all
                   .order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    elsif filter == 'my_recipes'
      @recipes = Recipe.where(user_id: current_user.id)
                   .order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    elsif filter == 'assigned_to_me'
      @posts = Post.where(user_id: current_user.id)
                   .order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
    elsif filter == 'with_comments'
      @posts = Post.joins(:comment_blocks).distinct.where('comment_blocks.user_id' => current_user.id).order("created_at DESC")
      @recipes = Recipe.joins(:comment_blocks).distinct.where('comment_blocks.user_id' => current_user.id).order("created_at DESC")

    end
  end
  def black_list
    @blacklists = Blacklist.all.order("created_at DESC").paginate(:page => params[:page], :per_page => 10)
  end

  def add_black_list
    @blacklist = Blacklist.new(blacklist_params_params)
    if
    @blacklist.save
      redirect_to black_list_domain_path, notice: "New domain has been added to blacklist"
    else
      render 'admin_panel/black_list'
      end
  end

  def destroy_black_list
    @blacklist = Blacklist.find(params[:id])
    @blacklist.destroy
    redirect_to black_list_domain_path, notice: "Domain has been deleted from blacklist"
  end

  private
  def blacklist_params_params
    params.fetch(:blacklist).permit!
  end
end
