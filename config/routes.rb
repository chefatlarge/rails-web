require 'domain_constraint'

Rails.application.routes.draw do
  constraints DomainConstraint.new('dima') do
    root :to => 'recipes#index'
  end
  post '/rate' => 'rater#create', :as => 'rate'

  resources :recipes do
    resources :comment_blocks
  end

  resources :posts do
    resources :comment_blocks
  end

  resources :foodiye_posts , :path => "pins" do
    resources :comment_blocks
  end


  resources :post_collaborators
  resources :user_collaborators

  mount Ckeditor::Engine => '/ckeditor'
  root 'posts#index'
  get 'users' => 'foodiye_posts#members'
  get 'get_title' => 'foodiye_posts#get_title'
  get 'hand/:id' => 'handler#hand'
  get 'tags/:tag', to: 'posts#index', as: 'tag'
  get 'authors/:user_nicename' => 'posts#authors', as: 'authors'
  get 'category/:slug' => 'posts#categories', as: 'category'
  post 'versions/:id/revert' => 'versions#revert', :as => 'revert_version'
  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  resources :users, :only => [:show]
  get '/posts/versions/history/:id' => 'versions#history', as: :posts_history
  get '/posts/:id/edit/:version_id' => 'posts#edit', as: :posts_version
  get '/recipes/:id/edit/:version_id' => 'recipes#edit', as: :recipes_version

  scope  'admin' do
  get '/dashboard' => 'admin_panel#main_page'
  get '/black_list_domain' => 'admin_panel#black_list'
  post '/black_list_domain' => 'admin_panel#add_black_list'
  delete '/black_list_domain' => 'admin_panel#destroy_black_list', :as => 'destroy'
  end

  get 'results', to: 'results#index', as: 'results'

  mount SimpleFormMarkdownEditor::Engine => "/"
  #reputation system
  delete  'likes/:resource_name/:resource_id' => 'likes#destroy', :as => 'unlike'
  post    'likes/:resource_name/:resource_id' => 'likes#create',  :as => 'like'

end
