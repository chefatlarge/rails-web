PgSearch.multisearch_options = {
    :using => {
                          dmetaphone: {
                              dictionary: 'english',
                              tsvector_column: 'tsv'
                          },
                          tsearch: {
                              dictionary: 'english',
                              tsvector_column: 'tsv'
                          }
                         }
}