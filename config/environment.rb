# Load the Rails application.
require_relative 'application'

DATABASE = YAML::load_file("#{Rails.root}/config/database.yml")[Rails.env]

# Initialize the Rails application.
Rails.application.initialize!
