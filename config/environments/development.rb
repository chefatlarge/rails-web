Rails.application.configure do
  config.cache_classes = true
  config.serve_static_assets = true
  config.assets.compile = true
  config.assets.digest = true
  require 'rest-client'


  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  if Rails.root.join('tmp/caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => 'public, max-age=172800'
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # config.action_mailer.raise_delivery_errors = false
  # config.action_mailer.perform_caching = false
  # config.action_mailer.delivery_method = :smtp
  # config.action_mailer.smtp_settings = {
  #     :authentication => :plain,
  #     :address => "smtp.mailgun.org",
  #     :port => 587,
  #     domain: 'postmaster@sandbox25d2e459390e4d86bed8de575b742029.mailgun.org',
  #     user_name: DATABASE['mailgun_user_name'],
  #     password: DATABASE['mailgun_password']
  # }

  #mailer (mailgun)
  config.action_mailer.delivery_method = :mailgun
  config.action_mailer.mailgun_settings = {
    api_key: 'key-19f094ca0268f43720c9b200edce3e48',   #DATABASE['mailgun_api-key']
    domain: 'mailers.chefatlarge.in'               #DATABASE['mailgun_domain']
  }

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  config.paperclip_defaults = {
      storage: :s3,
      s3_region: DATABASE['region'],
      s3_credentials: {
          bucket: DATABASE['bucket'],
          access_key_id: DATABASE['access_key_id'],
          secret_access_key: DATABASE['secret_access_key'],
      },
      s3_host_name: 's3-' + DATABASE['region'] + '.amazonaws.com',
  }

  Paperclip::Attachment.default_options[:path] = DATABASE['path'] + '/:class/:attachment/:id_partition/:style/:filename'

  # config.paperclip_defaults = {
  #     :storage => :s3,
  #     :url => ':s3_alias_url',
  #     s3_region: DATABASE['region'],
  #     :s3_host_alias => 'chefatlarge.cloudfront.net',
  #     :path =>  DATABASE['path'] + '/:class/:attachment/:id_partition/:style/:filename',
  #     :s3_credentials => {
  #         bucket: DATABASE['bucket'],
  #         access_key_id: DATABASE['access_key_id'],
  #         secret_access_key: DATABASE['secret_access_key'],
  #     }
  # }

  # replace this with your tracker code Google analytics
  GA.tracker = DATABASE['tracking_id']
end
