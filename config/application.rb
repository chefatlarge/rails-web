require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)


module ChefAtLarge
  class Application < Rails::Application
    config.serve_static_assets = true
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.active_record.schema_format = :sql

    # ckeditor
    config.assets.precompile += Ckeditor.assets
    config.assets.precompile += %w( ckeditor/* )
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    #sidekiq

    config.active_job.queue_adapter = :sidekiq

  end


end
