normal_role = Role.find_by(name: "normal")
unless normal_role
  normal_role = Role.new
  normal_role.name = "normal"
  normal_role.save!
end

admin_role = Role.find_by(name: "admin")
unless admin_role
  admin_role = Role.new
  admin_role.name = "admin"
  admin_role.save!
end

editor_role = Role.find_by(name: "editor")
unless editor_role
  editor_role = Role.new
  editor_role.name = "editor"
  editor_role.save!
end

writer_role = Role.find_by(name: "writer")
unless writer_role
  writer_role = Role.new
  writer_role.name = "writer"
  writer_role.save!
end


admin = User.find_by(email: 'admin@test.com')
unless admin
  admin = User.new
  admin.email = 'admin@test.com'
  admin.password = '12345678'
  admin.role_id = admin_role.id
  admin.user_nicename = 'test_admin'
  admin.save!
end

author = User.find_by(email: 'author@test.com')
unless author
  author = User.new
  author.email = 'author@test.com'
  author.password = '12345678'
  author.role_id = editor_role.id
  admin.user_nicename = 'test_editor'
  author.save!
end


Post.create(title: 'hello world', )