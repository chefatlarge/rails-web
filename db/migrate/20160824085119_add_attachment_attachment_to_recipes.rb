class AddAttachmentAttachmentToRecipes < ActiveRecord::Migration
  def self.up
    change_table :recipes do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :recipes, :attachment
  end
end
