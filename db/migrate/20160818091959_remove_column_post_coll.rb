class RemoveColumnPostColl < ActiveRecord::Migration[5.0]
  def self.up
    remove_column :post_collaborators, :user_id
    rename_column :post_collaborators, :user_collaborator_id, :post_collaborator_id
  end
  def self.down
    add_column    :post_collaborators, :user_id
    rename_column :post_collaborators, :post_collaborator_id, :user_collaborator_id
  end
end
