class CreateAttachments < ActiveRecord::Migration[5.0]
  def change
    create_table :attachments do |t|
      t.attachment  :attachment
      t.string  :quid, null: false, limit: 255
      t.references  :user, index: true
      t.references  :post
      t.timestamps
    end
  end
end
