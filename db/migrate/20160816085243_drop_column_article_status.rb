class DropColumnArticleStatus < ActiveRecord::Migration[5.0]
  def change
    remove_column :posts, :article_status
  end
end
