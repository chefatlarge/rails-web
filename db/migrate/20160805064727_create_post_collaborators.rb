class CreatePostCollaborators < ActiveRecord::Migration[5.0]
  def change
    create_table :post_collaborators do |t|
      t.references :post
      t.references :user
      t.string     :role
      t.boolean    :confirmation
      t.timestamps
    end
  end
end
