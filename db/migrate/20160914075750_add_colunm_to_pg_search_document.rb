class AddColunmToPgSearchDocument < ActiveRecord::Migration[5.0]
  def change
    add_column :pg_search_documents, :title, :string
    add_column :pg_search_documents, :name, :string
    add_column :pg_search_documents, :user_nicename, :string

  end
end
