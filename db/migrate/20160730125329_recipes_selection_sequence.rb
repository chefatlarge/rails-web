class RecipesSelectionSequence < ActiveRecord::Migration[5.0]
  def up
    connection.execute(%q{
            select setval('recipes_id_seq', max(id))
            from recipes
        })
  end
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
