class AddTsvectorColumnsToPost < ActiveRecord::Migration[5.0]
  def up
    add_column :posts, :tsv, :tsvector
    add_index :posts, :tsv, using: "gin"
    add_column :recipes, :tsv, :tsvector
    add_index :recipes, :tsv, using: "gin"

    execute <<-SQL
      CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
      ON posts FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger(
        tsv, 'pg_catalog.english', title, name, content
      );
    SQL


    execute <<-SQL
      CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
      ON recipes FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger(
        tsv, 'pg_catalog.english', title, name, content
      );
    SQL

    now = Time.current.to_s(:db)
    update("UPDATE posts SET updated_at = '#{now}'")

    now = Time.current.to_s(:db)
    update("UPDATE recipes SET updated_at = '#{now}'")
  end

  def down
    execute <<-SQL
      DROP TRIGGER tsvectorupdate
      ON posts
    SQL

    execute <<-SQL
      DROP TRIGGER tsvectorupdate
      ON recipes
    SQL

    remove_index :posts, :tsv
    remove_column :posts, :tsv
    remove_index :recipes, :tsv
    remove_column :recipes, :tsv
  end
end
