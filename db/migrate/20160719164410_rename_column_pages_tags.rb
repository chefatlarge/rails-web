class RenameColumnPagesTags < ActiveRecord::Migration[5.0]
  def change
    rename_column :pages_tags, :pages_id, :page_id
  end
end
