class AddColumnMainImageToAttachments < ActiveRecord::Migration[5.0]
  def change
    add_column :attachments, :main_image, :string
  end
end
