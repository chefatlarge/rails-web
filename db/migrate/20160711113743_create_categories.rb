class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.references :category
      t.string :name
      t.string :nicename
      t.timestamps
    end
  end
end
