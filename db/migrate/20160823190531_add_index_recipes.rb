class AddIndexRecipes < ActiveRecord::Migration[5.0]
  def change
    add_index :recipes, :slug
  end
end
