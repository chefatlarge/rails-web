class CreateCommentBlocks < ActiveRecord::Migration[5.0]
  def change
    create_table :comment_blocks do |t|

      t.text :body
      t.references :post
      t.references :user
      t.string     :commenter

      t.timestamps
    end

    #add_index :comment_blocks, :post_id
  end
end
