class RemiveExcerptNullFalse < ActiveRecord::Migration[5.0]
  def change
    change_column :recipes, :excerpt, :string, :null => true
    change_column :pages, :excerpt, :string, :null => true
  end
end
