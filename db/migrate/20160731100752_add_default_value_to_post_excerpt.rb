class AddDefaultValueToPostExcerpt < ActiveRecord::Migration[5.0]
    change_column :posts, :excerpt, :string, :null => true
end
