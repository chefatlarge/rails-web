class AddSecretToPost < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :secret, :string
    remove_column :post_collaborators, :role
  end
end
