class PostsSelectionSequence < ActiveRecord::Migration[5.0]

  def up
    connection.execute(%q{
            select setval('posts_id_seq', max(id))
            from posts
        })
  end
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
