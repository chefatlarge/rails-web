class AddCategory < ActiveRecord::Migration[5.0]
  def change
    add_reference :posts, :category, index: true
    add_reference :pages, :category, index: true
    add_reference :recipes, :category, index: true

  end
end
