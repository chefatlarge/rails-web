class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
      t.references :user, index: true
      t.references :post, null: false, default: 0, unique: true, index: true
      t.string  :title, null: false
      t.string  :name, null: false, limit: 200, index: true
      t.text    :content, null: false
      t.string  :excerpt, null: false
      t.string  :post_status, null: false, limit: 20, default: 'publish'
      t.string  :comment_status, null: false, limit: 20, default: 'open'
      t.string  :ping_status, null: false, limit: 20, default: 'open'
      t.text    :pinged, null: false
      t.string  :quid, null: false, limit: 255
      t.integer :menu_order, null: false, default: 0
      t.string  :post_type, null: false, limit: 20, default: 'post'
      t.string  :post_mime_type, null: false, limit: 100
      t.integer :comment_count, null: false, default: 0
      t.index   [:post_type, :post_status, :created_at, :id]



      t.timestamps
    end
  end
end
