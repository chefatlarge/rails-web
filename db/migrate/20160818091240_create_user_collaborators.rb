class CreateUserCollaborators < ActiveRecord::Migration[5.0]
  def change
    create_table :user_collaborators do |t|
      t.references :user
      t.references :post_collaborator
      t.timestamps
    end

  end


end
