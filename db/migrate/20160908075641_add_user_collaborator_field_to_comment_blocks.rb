class AddUserCollaboratorFieldToCommentBlocks < ActiveRecord::Migration[5.0]
  def up
    add_column :comment_blocks, :collaborate_user, :string

  end

  def down
    remove_column :comment_blocks, :collaborate_user, :string
  end
end
