class CreateBlacklists < ActiveRecord::Migration[5.0]
  def change
    create_table :blacklists do |t|
      t.string :domain
      t.timestamps
    end
  end
end
