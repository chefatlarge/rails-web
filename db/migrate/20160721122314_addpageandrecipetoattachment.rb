class Addpageandrecipetoattachment < ActiveRecord::Migration[5.0]
  def change
    add_reference :attachments, :page, index: true
    add_reference :attachments, :recipe, index: true
  end
end
