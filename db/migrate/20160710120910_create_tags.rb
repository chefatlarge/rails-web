class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.string  :name, null: false, limit: 200, index: true
      t.string  :slug, null: false, limit: 200, index: true
      t.index   :id
      t.timestamps
    end
  end
end
