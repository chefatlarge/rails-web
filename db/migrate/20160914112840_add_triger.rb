class AddTriger < ActiveRecord::Migration[5.0]
  def change
    say_with_time("Dropping table for pg_search multisearch") do
      drop_table :pg_search_documents
    end

    say_with_time("Creating table for pg_search multisearch") do
      create_table :pg_search_documents do |t|
        t.text :content
        t.string :name
        t.string :title
        t.belongs_to :searchable, :polymorphic => true, :index => true
        t.timestamps null: false
      end
    end
  end
end
