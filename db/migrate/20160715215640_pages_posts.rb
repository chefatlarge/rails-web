class PagesPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :pages_tags, id: false do |t|
      t.belongs_to :pages, index: true
      t.belongs_to :tag, index: true
    end
  end
end
