class AddShedulingFieldPostAndRecipe < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :schedule_date,:date
    add_column :recipes, :schedule_date,:date
  end
end
