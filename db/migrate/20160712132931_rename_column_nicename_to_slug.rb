class RenameColumnNicenameToSlug < ActiveRecord::Migration[5.0]
  def change
    rename_column :categories, :nicename, :slug
  end
end
