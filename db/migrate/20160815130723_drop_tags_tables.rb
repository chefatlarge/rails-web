class DropTagsTables < ActiveRecord::Migration[5.0]
  def change
    drop_table :pages_tags
    drop_table :posts_tags
    drop_table :recipes_tags
    drop_table :tags
  end
end
