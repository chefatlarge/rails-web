class CreateFoodiyePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :foodiye_posts do |t|
      t.references :user
      t.string    :title
      t.text      :message
      t.text      :description
      t.string    :photo_link
      t.string    :video_url
      t.string    :post_type
      t.timestamps
    end
  end
end
