class AddColumnUserOwnerToPostCalobarator < ActiveRecord::Migration[5.0]
  def change
    rename_column :post_collaborators, :user_id, :user_collaborator_id
    add_column    :post_collaborators, :user_id, :integer
  end
end
