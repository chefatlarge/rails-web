class AddAttachmentToFoodiyePost < ActiveRecord::Migration[5.0]
  def up
    add_attachment :foodiye_posts, :attachment
  end

  def down
    remove_attachment :foodiye_posts, :attachment
  end
end
