class AddColumnToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :user_login, :string, limit: 60, index: true
    add_column :users, :user_nicename, :string, index: true
    add_column :users, :user_url, :string, limit: 100
    add_column :users, :user_activation_key, :string
    add_column :users, :user_status, :integer, default: 0
    add_column :users, :display_name, :string
    add_column :users, :fb_connect_last_login, :integer, defailt: 0
    add_column :users, :fb_connect_user_id, :string, default: '0'

  end
end
