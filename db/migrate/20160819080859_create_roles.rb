class CreateRoles < ActiveRecord::Migration[5.0]
  def change
    create_table :roles do |t|
      t.string :name
      t.string :description

      t.timestamps null: false
    end

    add_reference :users, :role, index: true, foreign_key: true
    remove_column :users, :role
  end
end
