class UpdateTrigger < ActiveRecord::Migration[5.0]

    def change
      remove_column :pg_search_documents, :user_nicename
      remove_column :pg_search_documents, :tsv
    end

end
