class AddArticleStatusToPosts < ActiveRecord::Migration[5.0]
  def change
    add_column :posts, :article_status, :string, default: 'in-progress', null: false
  end
end
