class AddFacebookImage < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :fb_image_url, :string
  end

  def down
    remove_column :users, :fb_image_url, :string
  end
end
