class AddreferenceUserColl < ActiveRecord::Migration[5.0]
  def change
    add_reference :post_collaborators, :user_collaborator, index: true
    remove_column :post_collaborators, :post_collaborator_id
  end
end
