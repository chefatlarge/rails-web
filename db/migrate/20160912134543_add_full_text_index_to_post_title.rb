class AddFullTextIndexToPostTitle < ActiveRecord::Migration[5.0]

    def up
      execute "CREATE INDEX ON posts USING gin(to_tsvector('english', title));"
    end

    def down
      execute "DROP INDEX posts_to_tsvector_idx"
    end

end
