class AddSecretToRecipe < ActiveRecord::Migration[5.0]
  def change
    add_column :recipes, :secret, :string
  end
end
