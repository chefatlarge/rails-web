class AddRecipeFields < ActiveRecord::Migration[5.0]
  def change
    add_column :recipes, :short_description, :string
    add_column :recipes, :course, :string
    add_column :recipes, :cuisine, :string
    add_column :recipes, :price, :string
    add_column :recipes, :skill_level, :string
    add_column :recipes, :servings, :integer
    add_column :recipes, :prep_time, :integer
    add_column :recipes, :cook_time, :integer
    add_column :recipes, :ingredient, :json
    add_column :recipes, :instruction, :json

    add_reference :comment_blocks, :recipe
    add_reference :post_collaborators, :recipe
  end
end
