class AddFoodiyePostsIdToCommentBlocks < ActiveRecord::Migration[5.0]
  def up
    add_attachment :comment_blocks, :attachment
    add_reference :comment_blocks, :foodiye_post
  end

  def down
    remove_attachment :comment_blocks, :attachment
    remove_reference :comment_blocks, :foodiye_post
  end
end
