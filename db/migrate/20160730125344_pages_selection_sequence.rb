class PagesSelectionSequence < ActiveRecord::Migration[5.0]
  def up
    connection.execute(%q{
            select setval('pages_id_seq', max(id))
            from pages
        })
  end
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
