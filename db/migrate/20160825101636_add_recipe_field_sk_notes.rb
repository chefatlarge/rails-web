class AddRecipeFieldSkNotes < ActiveRecord::Migration[5.0]
  def up
    add_column :recipes, :note, :string
  end

  def down
    remove_column :recipes, :note
  end
end
