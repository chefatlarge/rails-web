class AddFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :short_description, :string
    add_column :users, :facebook_link, :string
    add_column :users, :instagram_link, :string
    add_column :users, :linkedin_link, :string
    add_column :users, :pinterest_link, :string
    add_column :users, :twitter_link, :string
    add_column :users, :public, :boolean, default: false
    add_column :users, :phone_number, :string
  end
end
