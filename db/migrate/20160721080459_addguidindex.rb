class Addguidindex < ActiveRecord::Migration[5.0]
  def change
    add_index :attachments, :quid
  end
end
