class AddTsvToUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :tsv_users, :tsvector
    add_index :users, :tsv_users, using: "gin"


    execute <<-SQL
     CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
     ON users FOR EACH ROW EXECUTE PROCEDURE
     tsvector_update_trigger(
       tsv_users, 'pg_catalog.english', user_nicename, email
     );
    SQL


    now = Time.current.to_s(:db)
    update("UPDATE users SET updated_at = '#{now}'")

  end

  def down
    execute <<-SQL
     DROP TRIGGER tsvectorupdate
     ON users
    SQL


    remove_index :users, :tsv_users, :tsvector
    remove_column :users, :tsv_users, using: "gin"
  end
end
