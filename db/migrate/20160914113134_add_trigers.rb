class AddTrigers < ActiveRecord::Migration[5.0]
  def up
    add_column :pg_search_documents, :tsv, :tsvector
    add_index :pg_search_documents, :tsv, using: "gin"



    execute <<-SQL
     CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
     ON pg_search_documents FOR EACH ROW EXECUTE PROCEDURE
     tsvector_update_trigger(
       tsv, 'pg_catalog.english', content, title, name
     );
    SQL


    now = Time.current.to_s(:db)
    update("UPDATE pg_search_documents SET updated_at = '#{now}'")

  end

  def down
    execute <<-SQL
     DROP TRIGGER tsvectorupdate
     ON pg_search_documents
    SQL


    remove_index :pg_search_documents, :tsv
    remove_column :pg_search_documents, :tsv
  end
end
