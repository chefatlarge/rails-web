class Removeslug < ActiveRecord::Migration[5.0]
  def change
   remove_column :recipes, :slug, :string
  end
end
