class RemoveRecipesPagesNullFalseColumn < ActiveRecord::Migration[5.0]
  def change
    change_column :recipes, :pinged, :text, :null => true
    change_column :recipes, :post_mime_type, :string, :null => true
    change_column :recipes, :quid, :string, :null => true
    change_column :recipes, :menu_order, :integer, :null => true
    change_column :pages, :pinged, :text, :null => true
    change_column :pages, :post_mime_type, :string, :null => true
    change_column :pages, :quid, :string, :null => true
    change_column :pages, :menu_order, :integer, :null => true
  end
end
