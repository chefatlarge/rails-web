class CorrectUserSeq < ActiveRecord::Migration[5.0]
  def up
    connection.execute(%q{
            select setval('users_id_seq', max(id))
            from users
        })
  end
  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
