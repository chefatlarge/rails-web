# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160908075641) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"
  enable_extension "fuzzystrmatch"

  create_table "attachments", force: :cascade do |t|
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "quid",                    limit: 255, null: false
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "main_image"
    t.integer  "page_id"
    t.integer  "recipe_id"
    t.index ["page_id"], name: "index_attachments_on_page_id", using: :btree
    t.index ["post_id"], name: "index_attachments_on_post_id", using: :btree
    t.index ["quid"], name: "index_attachments_on_quid", using: :btree
    t.index ["recipe_id"], name: "index_attachments_on_recipe_id", using: :btree
    t.index ["user_id"], name: "index_attachments_on_user_id", using: :btree
  end

  create_table "average_caches", force: :cascade do |t|
    t.integer  "rater_id"
    t.string   "rateable_type"
    t.integer  "rateable_id"
    t.float    "avg",           null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: :cascade do |t|
    t.integer  "parent_category_id"
    t.string   "name"
    t.string   "slug"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["parent_category_id"], name: "index_categories_on_parent_category_id", using: :btree
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree
  end

  create_table "comment_blocks", force: :cascade do |t|
    t.text     "body"
    t.integer  "post_id"
    t.integer  "user_id"
    t.string   "commenter"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "recipe_id"
    t.string   "collaborate_user"
    t.index ["post_id"], name: "index_comment_blocks_on_post_id", using: :btree
    t.index ["recipe_id"], name: "index_comment_blocks_on_recipe_id", using: :btree
    t.index ["user_id"], name: "index_comment_blocks_on_user_id", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "overall_averages", force: :cascade do |t|
    t.string   "rateable_type"
    t.integer  "rateable_id"
    t.float    "overall_avg",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id",                    default: 0,         null: false
    t.string   "title",                                          null: false
    t.string   "name",           limit: 200,                     null: false
    t.text     "content",                                        null: false
    t.string   "excerpt"
    t.string   "post_status",    limit: 20,  default: "publish", null: false
    t.string   "comment_status", limit: 20,  default: "open",    null: false
    t.string   "ping_status",    limit: 20,  default: "open",    null: false
    t.text     "pinged"
    t.string   "quid"
    t.integer  "menu_order",                 default: 0
    t.string   "post_type",      limit: 20,  default: "post",    null: false
    t.string   "post_mime_type"
    t.integer  "comment_count",              default: 0,         null: false
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "category_id"
    t.index ["category_id"], name: "index_pages_on_category_id", using: :btree
    t.index ["name"], name: "index_pages_on_name", using: :btree
    t.index ["post_id"], name: "index_pages_on_post_id", using: :btree
    t.index ["post_type", "post_status", "created_at", "id"], name: "index_pages_on_post_type_and_post_status_and_created_at_and_id", using: :btree
    t.index ["user_id"], name: "index_pages_on_user_id", using: :btree
  end

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.string   "searchable_type"
    t.integer  "searchable_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree
  end

  create_table "post_collaborators", force: :cascade do |t|
    t.integer  "post_id"
    t.boolean  "confirmation"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_collaborator_id"
    t.integer  "recipe_id"
    t.index ["post_id"], name: "index_post_collaborators_on_post_id", using: :btree
    t.index ["recipe_id"], name: "index_post_collaborators_on_recipe_id", using: :btree
    t.index ["user_collaborator_id"], name: "index_post_collaborators_on_user_collaborator_id", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id",                    default: 0,         null: false
    t.string   "title",                                          null: false
    t.string   "name",           limit: 200,                     null: false
    t.text     "content",                                        null: false
    t.string   "excerpt"
    t.string   "post_status",    limit: 20,  default: "publish", null: false
    t.string   "comment_status", limit: 20,  default: "open",    null: false
    t.string   "ping_status",    limit: 20,  default: "open",    null: false
    t.text     "pinged"
    t.string   "quid",           limit: 255
    t.integer  "menu_order",                 default: 0
    t.string   "post_type",      limit: 20,  default: "post",    null: false
    t.string   "post_mime_type", limit: 100
    t.integer  "comment_count",              default: 0,         null: false
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "category_id"
    t.string   "slug"
    t.string   "secret"
    t.date     "schedule_date"
    t.index ["category_id"], name: "index_posts_on_category_id", using: :btree
    t.index ["name"], name: "index_posts_on_name", using: :btree
    t.index ["post_id"], name: "index_posts_on_post_id", using: :btree
    t.index ["post_type", "post_status", "created_at", "id"], name: "index_posts_on_post_type_and_post_status_and_created_at_and_id", using: :btree
    t.index ["slug"], name: "index_posts_on_slug", unique: true, using: :btree
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "rates", force: :cascade do |t|
    t.integer  "rater_id"
    t.string   "rateable_type"
    t.integer  "rateable_id"
    t.float    "stars",         null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["rateable_id", "rateable_type"], name: "index_rates_on_rateable_id_and_rateable_type", using: :btree
    t.index ["rater_id"], name: "index_rates_on_rater_id", using: :btree
  end

  create_table "rating_caches", force: :cascade do |t|
    t.string   "cacheable_type"
    t.integer  "cacheable_id"
    t.float    "avg",            null: false
    t.integer  "qty",            null: false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["cacheable_id", "cacheable_type"], name: "index_rating_caches_on_cacheable_id_and_cacheable_type", using: :btree
  end

  create_table "recipes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id",                             default: 0,         null: false
    t.string   "title",                                                   null: false
    t.string   "name",                    limit: 200,                     null: false
    t.text     "content",                                                 null: false
    t.string   "excerpt"
    t.string   "post_status",             limit: 20,  default: "publish", null: false
    t.string   "comment_status",          limit: 20,  default: "open",    null: false
    t.string   "ping_status",             limit: 20,  default: "open",    null: false
    t.text     "pinged"
    t.string   "quid"
    t.integer  "menu_order",                          default: 0
    t.string   "post_type",               limit: 20,  default: "post",    null: false
    t.string   "post_mime_type"
    t.integer  "comment_count",                       default: 0,         null: false
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.integer  "category_id"
    t.string   "short_description"
    t.string   "course"
    t.string   "cuisine"
    t.string   "passive_time"
    t.string   "skill_level"
    t.integer  "servings"
    t.integer  "prep_time"
    t.integer  "cook_time"
    t.json     "ingredient"
    t.json     "instruction"
    t.string   "slug"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "note"
    t.string   "secret"
    t.date     "schedule_date"
    t.index ["category_id"], name: "index_recipes_on_category_id", using: :btree
    t.index ["name"], name: "index_recipes_on_name", using: :btree
    t.index ["post_id"], name: "index_recipes_on_post_id", using: :btree
    t.index ["post_type", "post_status", "id"], name: "index_recipes_on_post_type_and_post_status_and_id", using: :btree
    t.index ["slug"], name: "index_recipes_on_slug", using: :btree
    t.index ["user_id"], name: "index_recipes_on_user_id", using: :btree
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
  end

  create_table "user_collaborators", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_collaborator_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["post_collaborator_id"], name: "index_user_collaborators_on_post_collaborator_id", using: :btree
    t.index ["user_id"], name: "index_user_collaborators_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                              default: "",    null: false
    t.string   "encrypted_password",                 default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "user_login",             limit: 60
    t.string   "user_nicename"
    t.string   "user_url",               limit: 100
    t.string   "user_activation_key"
    t.integer  "user_status",                        default: 0
    t.string   "display_name"
    t.integer  "fb_connect_last_login"
    t.string   "fb_connect_user_id",                 default: "0"
    t.integer  "role_id"
    t.string   "provider"
    t.string   "uid"
    t.string   "short_description"
    t.string   "facebook_link"
    t.string   "instagram_link"
    t.string   "linkedin_link"
    t.string   "pinterest_link"
    t.string   "twitter_link"
    t.boolean  "public",                             default: false
    t.string   "phone_number"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["role_id"], name: "index_users_on_role_id", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
  end

  add_foreign_key "users", "roles"
end
