namespace :wordpress do
  task delete_pg: :environment do
    User.delete_all
    Post.delete_all
    Category.delete_all
    Tag.delete_all
    Page.delete_all
    Recipe.delete_all
    Attachment.delete_all
  end
  task import_sql: :environment do
    def create_or_update(cls, id)
      begin
        cls.find(id)
      rescue ActiveRecord::RecordNotFound
        cls.new
      end
    end

    puts "Process tags"
    WpTerm.all.each do |t|
      unless t.wp_term_taxonomy.blank?
        taxonomy = t.wp_term_taxonomy.taxonomy
        parent =   t.wp_term_taxonomy.parent
        if taxonomy == 'post_tag'
          tag = create_or_update(Tag, t.term_id)
          tag.id = t.term_id
          tag.name = t.name
          tag.slug = t.slug
          tag.save!
        elsif taxonomy == 'category'
          category = create_or_update(Category, t.term_id)
          category.id = t.term_id
          category.name = t.name
          category.slug = t.slug
          unless parent == 0
            category.parent_category_id = parent
          end
          category.save!
        end
      end
    end

    puts "Process authors"
    WpAuthor.all.each do |t|
      author = create_or_update(User, t.id)
      author.id = t.id
      author.user_login = t.user_login
      author.password = t.user_pass
      author.user_nicename = t.user_nicename
      author.email = t.user_email
      author.user_url = t.user_url
      author.created_at = t.user_registered
      author.user_activation_key = t.user_activation_key
      author.user_status = t.user_status
      author.display_name = t.display_name
      author.fb_connect_last_login = t.fbconnect_lastlogin
      author.fb_connect_user_id = t.fbconnect_userid
      unless author.email.blank?
        author.save!
      end
    end

    puts "Process posts"
    WpPost.all.each do |t|
      post_type = t.post_type
      def post_save(post,t)
        post.id = t.id
        post.user_id = t.post_author
        post.created_at = t.post_date
        post.content = t.post_content
        #post.content = ReverseMarkdown.convert post.content
        post.title = t.post_title
        post.excerpt = t.post_excerpt
        post.post_status = t.post_status
        post.comment_status = t.comment_status
        post.ping_status = t.ping_status
        post.name = t.post_name
        post.pinged = t.pinged
        post.updated_at = t.post_modified
        post.post_id = t.post_parent
        post.quid = t.guid
        post.menu_order = t.menu_order
        post.post_mime_type = t.post_mime_type
        post.comment_count = t.comment_count
        post.tags.clear
        t.tags.each do |id|
          post.tags.push Tag.find(id)
        end
        unless t.categories.empty?
          post.category = Category.find(t.categories.first)
        end
        post.save!
      end
      if post_type == 'post'
        post = create_or_update(Post, t.id)
        post_save(post,t)
      elsif post_type == 'page'
        post = create_or_update(Page, t.id)
        post_save(post,t)
      elsif post_type == 'recipe'
        post = create_or_update(Recipe, t.id)
        post_save(post,t)
      end
    end
  end

  task upload_s3: :environment do

    def parse_images(j)
      links = j.content.scan(/http:\/\/www.chefatlarge.in\/wp-content\/uploads\/[^ ")\]\[]+/)

      need_save = false
      links.each do |l|
        new_attachment = Attachment.new
        new_attachment.quid = l
        if j.class == Post
          new_attachment.post_id = j.id
        elsif j.class == Page
          new_attachment.page_id = j.id
        elsif j.class == Recipe
          new_attachment.recipe_id = j.id
        end

        new_attachment.main_image = "no"
        begin
          encoded_url = URI.encode(l)
          new_attachment.attachment = URI.parse(encoded_url)
          begin
            new_attachment.save!

            image = "/hand/#{new_attachment.id}"
            re = Regexp.new(Regexp.quote(l))
            j.content.gsub!(re, image)
            puts "File copied"
            need_save = true
          rescue ActiveRecord::RecordInvalid => e
            puts "Validation failed: #{e.message} for url #{l}"
          end
        rescue OpenURI::HTTPError => e
          puts "Error #{e.message} for url #{l}"
        end
      end

      if need_save
        j.save!
        puts "Saved post #{j.id}"
      end
    end

    puts "Process attachments in the post"
    Post.all.each do |j|
      parse_images j
    end

    puts "Process attachments in the page"
    Page.all.each do |j|
      parse_images j
    end

    puts "Process attachments in the recipe"
    Recipe.all.each do |j|
      parse_images j
    end

    puts "Process attachments from table post"
    WpPost.all.each do |t|
      post_parent = t.post_parent
      post_type = t.post_type
      if post_type == 'attachment' and post_parent != 0
        begin
          Attachment.find_by_id!(t.id)
          Attachment.find_by_quid!(t.guid)
        rescue ActiveRecord::RecordNotFound
          attachment = Attachment.new
          attachment.id = t.id
          attachment.user_id = t.post_author
          attachment.created_at = t.post_date
          attachment.updated_at = t.post_modified
          attachment.post_id = t.post_parent
          attachment.quid = t.guid
          attachment.main_image = "yes"
          begin
            encoded_url = URI.encode(t.guid)
            attachment.attachment = URI.parse(encoded_url)
            begin
              attachment.save!
              puts "Attachment saved"
            rescue ActiveRecord::RecordInvalid => e
              puts "Validation failed #{e.message} for url #{encoded_url}"
            rescue ActiveRecord::RecordNotUnique => e
              puts "Duplicate key value #{e.message} #{t.id}"
            end
          rescue OpenURI::HTTPError => e
            puts "Error #{e.message} for url #{t.guid}"
          end
        end
      end
    end
  end
end
