require 'test_helper'

class FoodiyePostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @foodiye_post = foodiye_posts(:one)
  end

  test "should get index" do
    get foodiye_posts_url
    assert_response :success
  end

  test "should get new" do
    get new_foodiye_post_url
    assert_response :success
  end

  test "should create foodiye_post" do
    assert_difference('FoodiyePost.count') do
      post foodiye_posts_url, params: { foodiye_post: {  } }
    end

    assert_redirected_to foodiye_post_url(FoodiyePost.last)
  end

  test "should show foodiye_post" do
    get foodiye_post_url(@foodiye_post)
    assert_response :success
  end

  test "should get edit" do
    get edit_foodiye_post_url(@foodiye_post)
    assert_response :success
  end

  test "should update foodiye_post" do
    patch foodiye_post_url(@foodiye_post), params: { foodiye_post: {  } }
    assert_redirected_to foodiye_post_url(@foodiye_post)
  end

  test "should destroy foodiye_post" do
    assert_difference('FoodiyePost.count', -1) do
      delete foodiye_post_url(@foodiye_post)
    end

    assert_redirected_to foodiye_posts_url
  end
end
