require 'test_helper'

class PostCollaboratorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post_collaborator = post_collaborators(:one)
  end

  test "should get index" do
    get post_collaborators_url
    assert_response :success
  end

  test "should get new" do
    get new_post_collaborator_url
    assert_response :success
  end

  test "should create post_collaborator" do
    assert_difference('PostCollaborator.count') do
      post post_collaborators_url, params: { post_collaborator: {  } }
    end

    assert_redirected_to post_collaborator_url(PostCollaborator.last)
  end

  test "should show post_collaborator" do
    get post_collaborator_url(@post_collaborator)
    assert_response :success
  end

  test "should get edit" do
    get edit_post_collaborator_url(@post_collaborator)
    assert_response :success
  end

  test "should update post_collaborator" do
    patch post_collaborator_url(@post_collaborator), params: { post_collaborator: {  } }
    assert_redirected_to post_collaborator_url(@post_collaborator)
  end

  test "should destroy post_collaborator" do
    assert_difference('PostCollaborator.count', -1) do
      delete post_collaborator_url(@post_collaborator)
    end

    assert_redirected_to post_collaborators_url
  end
end
