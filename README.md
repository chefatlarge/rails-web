To start local rake

1. If need, delete all data from local postgres database
rake wordpress:delete_pg
2. first rake download all data from Mysql, except attachment, because of copying of amazon s3 to db + convert to markdown.
rake wordpress:import_sql
3. second rake works with attachments, download and upload to amazon s3 nd write new url to database.
rake wordpress:upload_s3

To start Heroku rake

1. You need to connect Mysql bd
(your host, login, password in database.yml)
2. first rake: heroku run rake wordpress:import_sql
3. second rake: heroku run rake wordpress:upload_s3